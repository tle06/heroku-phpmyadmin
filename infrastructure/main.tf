/* -------------------------------------------------------------------------- */
/*                     Terraform state with gitlab backend                    */
/* -------------------------------------------------------------------------- */

terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5

  }
}

/* -------------------------------------------------------------------------- */
/*                                   locals                                   */
/* -------------------------------------------------------------------------- */

locals {
  heroku = {
    region = "eu"
  }
  app = {
    name               = "phpmyadmin"
    domain             = "tlnk.fr"
    azure_zone_rg_name = "TF_PROD_DNS_PUBLIC"

    prod = {
      heroku_formation_size = "free"
      dns_record_name       = "pma"

    }

    stage = {
      heroku_formation_size = "free"
      dns_record_name       = "pma.stage"
    }
  }
}

/* -------------------------------------------------------------------------- */
/*                                  Database                                  */
/* -------------------------------------------------------------------------- */

data "scaleway_rdb_instance" "database" {
  name = "prod-database01"
}

/* -------------------------------------------------------------------------- */
/*                                   Heroku                                   */
/* -------------------------------------------------------------------------- */

resource "heroku_app" "app" {
  name   = "${local.environment}-${local.app.name}"
  region = local.heroku.region

}

resource "heroku_app_config_association" "app" {
  app_id = heroku_app.app.id
  sensitive_vars = {
    ENV                  = local.environment
    DATABASE_HOST        = data.scaleway_rdb_instance.database.endpoint_ip
    DATABASE_PORT_NUMBER = data.scaleway_rdb_instance.database.endpoint_port

  }
}

resource "heroku_formation" "app_formation" {
  count    = local.app[local.environment].heroku_formation_size == "free" ? 0 : 1
  app      = heroku_app.app.name
  type     = "web"
  size     = local.app[local.environment].heroku_formation_size
  quantity = 1
}


resource "heroku_domain" "app_custom_domain" {
  app      = heroku_app.app.name
  hostname = "${local.app[local.environment].dns_record_name}.${local.app.domain}"
}

/* -------------------------------------------------------------------------- */
/*                                     DNS                                    */
/* -------------------------------------------------------------------------- */

resource "azurerm_dns_cname_record" "app_record" {
  name                = local.app[local.environment].dns_record_name
  zone_name           = local.app.domain
  resource_group_name = local.app.azure_zone_rg_name
  ttl                 = 300
  record              = heroku_domain.app_custom_domain.cname
}