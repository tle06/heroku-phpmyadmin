terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.0"
    }

    null = {
      source  = "hashicorp/null"
      version = "~> 3.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~>2.0"
    }
    heroku = {
      source  = "heroku/heroku"
      version = "4.8.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "2.2.0"
    }
  }

  required_version = ">= 1.0"
}