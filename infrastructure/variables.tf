locals {
  environment = lookup(var.workspace_to_environment_map, var.ENV, "stage")
}

/* -------------------------------------------------------------------------- */
/*                             environment mapping                            */
/* -------------------------------------------------------------------------- */

variable "workspace_to_environment_map" {
  description = "The map between workspace and environment"
  type        = map(any)
  default = {
    stage = "stage"
    prod  = "prod"
  }
}

variable "azure-default-location" {
  type        = string
  description = "The Azure default location"
  default     = "France Central"
}

variable "ENV" {
  description = "The ENV value"
  type        = string
}