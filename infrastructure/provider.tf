provider "azurerm" {
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}

provider "scaleway" {
  zone   = "fr-par-1"
  region = "fr-par"

}