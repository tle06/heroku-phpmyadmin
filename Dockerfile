FROM bitnami/phpmyadmin:5.1.1
LABEL maintainer="tle@tlnk.fr"

USER root

RUN sed -i -e '0,/APACHE_HTTP_PORT_NUMBER=/s/APACHE_HTTP_PORT_NUMBER=.*$/APACHE_HTTP_PORT_NUMBER="$PORT"/' /opt/bitnami/scripts/apache-env.sh

USER 1001

LABEL org.label-schema.name="tlnk.fr"
LABEL org.label-schema.description="phpmyadmin application"
LABEL org.label-schema.url="https://console.tlnk.fr"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://gitlab.com/tle06/heroku-phpmyadmin"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd=""